"use strict";
Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();

        var postImage = event.currentTarget.children[0].files[0];

        var postName = event.currentTarget.children[2].value;

        var postMessage = event.currentTarget.children[4].value;

        if (postName === "") {
            Materialize.toast('No task are being submitted', 4000)
        }

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                Materialize.toast('I am a toast!', 4000);
                return false;
            } else {
                Materialize.toast('Submit was succesful', 4000);
                Collections.Tasks.insert({
                    name: postName,
                    createdAt: new Date().toLocaleDateString(),
                    Message: postMessage,
                    imageId: fileObject._id
                });
                $('.grid').masonry('reloadItems');
            }


        });




    }

});

Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
});