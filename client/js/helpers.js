"use strict";
//the name value postList is the name and then it should run a function
Template.posts.helpers({
    postList: function () {
        return Collections.Tasks.find({}, {
            sort: {
                createdAt: -1
            }
        });

    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 15,
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });

    },

    imageIs: function (ImageId) {
        var image = Collections.Images.find({
            _id: ImageId
        }).fetch();
        return image[0].url();
    }



});